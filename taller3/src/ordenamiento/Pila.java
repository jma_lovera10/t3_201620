package ordenamiento;

import java.util.Iterator;
/**
 * Clase de pila basada en el "STACK" descrito en el libro Algorithms 4th ed. - R. Sedgewick, K. Wayne
 * @author Juan Manuel A. Lovera
 *
 * @param <T> Par�metro gen�rico
 */
public class Pila <T> implements Iterable<T>{

	/**
	 * Primer nodo de la pila
	 */
	private Nodo<T> primero;
	
	/**
	 * Tama�o de la pila
	 */
	private int size;
	
	/**
	 * M�todo que retorna el tama�o de la pila
	 * @return Size de la pila
	 */
	public int size(){
		return size;
	}
	
	/**
	 * M�todo que retorna el estado de la pila
	 * @return True si est� vacia, false en caso contrario
	 */
	public boolean isEmpty(){
		return size==0;
	}
	
	/**
	 * M�todo que agrega un elemento en el tope de la pila
	 * @param item Elemento que ser� contenido en el tope de la pila
	 */
	public void push(T item){
		Nodo<T> nuevo = new Nodo<T>(item);
		nuevo.siguiente = primero;
		primero = nuevo;
		size++;
	}
	
	/**
	 * M�todo que saca el elemento del tope de la pila
	 * @return Elemento del tope de la pila
	 */
	public T pop(){
		T elem = primero.elem;
		primero = primero.siguiente;
		size--;
		return elem;
	}

	public Iterator<T> iterator() {
		return new ListIterator<T>();
	}
	
	private class ListIterator<T> implements Iterator<T>{
		
		private Nodo<T> actual = (Nodo<T>) primero;
		
		public boolean hasNext(){
			return actual != null;
		}
		
		public T next() {
			T item = actual.elem;
			actual = actual.siguiente;
			return item;
		}
		
	}
}
