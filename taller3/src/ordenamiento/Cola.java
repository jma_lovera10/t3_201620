package ordenamiento;

import java.util.Iterator;
/**
 * Clase de cola basada en el "QUEUE" descrito en el libro Algorithms 4th ed. - R. Sedgewick, K. Wayne
 * @author Juan Manuel A. Lovera
 *
 * @param <T> Par�metro gen�rico
 */
public class Cola <T> implements Iterable<T>{

	/**
	 * Primer nodo de la cola
	 */
	private Nodo<T> primero;
	
	/**
	 * �ltimo nodo de la cola
	 */
	private Nodo<T> ultimo;
	
	/**
	 * Tama�o de la cola
	 */
	private int size;
	
	/**
	 * M�todo que retorna el tama�o de la cola
	 * @return Size de la cola
	 */
	public int size(){
		return size;
	}
	
	/**
	 * M�todo que retorna el estado de la cola
	 * @return True si est� vacia, false en caso contrario
	 */
	public boolean isEmpty(){
		return size==0;
	}
	
	/**
	 * M�todo que agrega un elemento al final de la cola
	 * @param item Elemento que ser� contenido al final de la cola
	 */
	public void enqueue(T item){
		Nodo<T> nuevo = new Nodo<T>(item);
		Nodo<T> elUltimo = ultimo;
		ultimo = nuevo; 
		if(isEmpty()) primero=ultimo;
		else  elUltimo.siguiente = ultimo;
		size++;
	}
	
	/**
	 * M�todo que saca el elemento de la cabeza de la cola
	 * @return Elemento de la cabeza de la cola
	 */
	public T dequeue(){
		T elem = primero.elem;
		primero = primero.siguiente;
		if(isEmpty())ultimo = null;
		size--;
		return elem;
	}

	public Iterator<T> iterator() {
		return new ListIterator<T>();
	}
	
	private class ListIterator<T> implements Iterator<T>{
		
		private Nodo<T> actual = (Nodo<T>) primero;
		
		public boolean hasNext(){
			return actual != null;
		}
		
		public T next() {
			T item = actual.elem;
			actual = actual.siguiente;
			return item;
		}
		
	}

}
