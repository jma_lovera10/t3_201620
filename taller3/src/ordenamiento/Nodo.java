package ordenamiento;

/**
 * Clase de nodo basada en el nodo descrito en el libro Algorithms 4th ed. - R. Sedgewick, K. Wayne
 * @author Juan Manuel A. Lovera
 *
 * @param <T> Parámetro genérico
 */
public class Nodo <T>{

	Nodo<T> siguiente;
	
	T elem;
	
	public Nodo(T elem) 
	{
		this.elem = elem;
	}
	 
}
