package parqueadero;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import ordenamiento.Cola;
import ordenamiento.Pila;


public class Central 
{
	/**
	 * Cola de carros en espera para ser estacionados
	 */
	private Cola<Carro> colaEspera;

	/**
	 * Pilas de carros parqueaderos 1, 2, 3 .... 8
	 */
	private List<Pila<Carro>> pilasParqueo;


	/**
	 * Pila de carros parqueadero temporal:
	 * Aca se estacionan los carros temporalmente cuando se quiere
	 * sacar un carro de un parqueadero y no es posible sacarlo con un solo movimiento
	 */
	private Pila<Carro> pilaTemporal;

	/**
	 * Inicializa el parqueadero: Los parqueaderos (1,2... 8) el estacionamiento temporal y la cola de carros que esperan para ser estacionados.
	 */
	public Central ()
	{
		colaEspera = new Cola<Carro>();
		pilasParqueo = new ArrayList<Pila<Carro>>();
		for (int i = 0; i < 8; i++) {
			pilasParqueo.add(new Pila<Carro>());
		}
		pilaTemporal = new Pila<Carro>();
	}

	/**
	 * Registra un cliente que quiere ingresar al parqueadero y el vehiculo ingresa a la cola de carros pendientes por parquear
	 * @param pColor color del vehiculo
	 * @param pMatricula matricula del vehiculo
	 * @param pNombreConductor nombre de quien conduce el vehiculo
	 */
	public void registrarCliente (String pColor, String pMatricula, String pNombreConductor)
	{
		colaEspera.enqueue(new Carro(pColor, pMatricula, pNombreConductor));
	}    

	/**
	 * Parquea el siguiente carro en la cola de carros por parquear
	 * @return matricula del vehiculo parqueado y ubicaci�n
	 * @throws Exception
	 */
	public String parquearCarroEnCola() throws Exception
	{	
		String respuesta = "";
		if(revisarDisponibilidad()){
			Carro car = colaEspera.dequeue();
			if(car!=null){
				respuesta = car.darMatricula();
				respuesta+=" : "+parquearCarro(car);
			}else{
				respuesta = "No hay carros en la cola de espera";
			}
		}
		else{
			throw new Exception("Por el momento no hay espacio disponible en el parqueadero.");
		}

		return respuesta;
	} 
	/**
	 * Saca del parqueadero el vehiculo de un cliente
	 * @param matricula del carro que se quiere sacar
	 * @return El monto de dinero que el cliente debe pagar
	 * @throws Exception si no encuentra el carro
	 */
	public String atenderClienteSale (String matricula) throws Exception
	{
		String resp = "";
		Carro car = sacarCarro(matricula);
		double tiempo = 0;
		double tarifa = 0;

		if(car!=null){
			tiempo = (System.currentTimeMillis()-car.darLlegada())/60000;
			tarifa = cobrarTarifa(tiempo);
			resp+="Duraci�n: "+tiempo+" minutos\n";
			resp+="Tarifa: $25\n";
			resp+="Costo: $"+tarifa;
		}else{
			resp = "No se encontr� el carro con la matr�cula: "+matricula;
		}
		return resp;
	}

	/**
	 * Busca un parqueadero co cupo dentro de los 8 existentes y parquea el carro
	 * @param aParquear es el carro que se saca de la cola de carros que estan esperando para ser parqueados
	 * @return El parqueadero en el que qued� el carro
	 * @throws Exception
	 */
	public String parquearCarro(Carro aParquear) throws Exception
	{
		String respuesta = "PARQUEADERO ";
		int i = 1;
		for (Pila<Carro> pila : pilasParqueo) {
			if(pila.size()<4){
				pila.push(aParquear);
				respuesta+=i;
				break;
			}
			i++;
		}
		return respuesta;    	    
	}

	/**
	 * Itera sobre los tres parqueaderos buscando uno con la placa ingresada
	 * @param matricula del vehiculo que se quiere sacar
	 * @return el carro buscado
	 */
	public Carro sacarCarro (String matricula)
	{
		Carro buscado = null;
		boolean esPila = false;
		int pos = -1;
		Iterator<Carro> iter;

		for (Pila<Carro> pila : pilasParqueo) {
			iter = pila.iterator();
			while(iter.hasNext()&&!esPila){
				Carro car = iter.next();
				esPila = car.darMatricula().equals(matricula);
				pos++;
			}
			if(esPila){
				while(pos>=0){
					pilaTemporal.push(pila.pop());
					pos--;
				}
				buscado = pilaTemporal.pop();
				while(!pilaTemporal.isEmpty()){
					pila.push(pilaTemporal.pop());
				}
				break;
			}
		}
		return buscado;
	}

	/**
	 * M�todo que revisa la diponibilidad en los parqueaderos
	 * @return Disponibilidad de los parqueaderos
	 */
	public boolean revisarDisponibilidad(){
		boolean hay = false;
		for (Pila<Carro> pila : pilasParqueo) {
			if(pila.size()<4){
				hay = true;
				break;
			}
		}
		return hay;
	}

	/**
	 * Calcula el valor que debe ser cobrado al cliente en funci�n del tiempo que dur� un carro en el parqueadero
	 * la tarifa es de $25 por minuto
	 * @param car recibe como parametro el carro que sale del parqueadero
	 * @return el valor que debe ser cobrado al cliente 
	 */
	public double cobrarTarifa (double tiempo)
	{
		return tiempo*25;	
	}

	/**
	 * M�todo que representa gr�ficamente el estado de los parqueaderos.
	 * @return Estado gr�fico de los parqueaderos
	 */
	public String verEstadoParqueaderos() {
		String resp = "";
		int i = 1;
		int j = 1;

		for (Pila<Carro> pila : pilasParqueo) {
			resp+="Pila"+i+": ";
			while(j<=4){
				resp+=pila.size()>=j?"X":"O";
				j++;
			}
			resp+="\n";
			j=1;
			i++;
		}

		return resp;
	}

	/**
	 * M�todo que representa gr�ficamente los carros en la cola
	 * @return Representaci�n gr�fica de carros en cola 
	 */
	public String carrosEnCola() {
		String resp = "-";

		Iterator<Carro> iter = colaEspera.iterator();
		while(iter.hasNext()){
			resp+="|"+iter.next().darMatricula()+"|-";
		}

		return resp;
	}
}
